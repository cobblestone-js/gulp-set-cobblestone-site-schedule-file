gulp-set-cobblestone-site-schedule-file
=======================================

> A *gulp* plugin for setting metadata schedule information from a site-wide schedule YAML.

## Setup and usage

Install `gulp-set-cobblestone-site-schedule-file` using `npm`:

```sh
npm i gulp-set-cobblestone-site-schedule-file
```

In your `gulpfile.js`:

```js
var gulp = require('gulp');
var schedule = require('gulp-set-cobblestone-site-schedule-file');

gulp.task('default', function() {
  return gulp.src('./src/**.*')
    .pipe(schedule({ scheduleFile: "schedule.yaml" }))
    .pipe(gulp.dest('./dest'));
});
```

A common workflow using this tool is to parse the files, then create a propery called `breadcrumbs` which has an ordered list of parent items, excluding the current one. This can then be walked in a template to write out the crumbs.

## Options

### property

*string*

Default: `data.schedule`

The name of the property (which can be nested) to put the resulting ordered list of breadcrumbs. The resulting item will
be an array of schedule data.

### scheduleFile

*string*

The name of the file to load the schedule data (see below). This or `scheduleYaml` is required.

### scheduleYaml

*string*

A block of YAML formatted data that contains the schedule data (see below). This or `scheduleFile` is required.

### logLevel

*number*

Defaults: `0`

Include increasingly more detailed levels of logging and tracing.

### skipIf

*{ (file: any) => boolean }*

If provided, allows a file-by-file override for the schedule.

## Schedule

The schedule file is a YAML file with RegExp keys for relative paths of a file (`data.relativePath`) with the value being information about that schedule. Any key/values or even nested structures are allowed here and will be push into the schedule block for the file except for three keys which are used for processing and will be removed.

### scheduleData (Required)

*string | Moment*

The date to start the schedule. If the key matches a single file, this will be the schedule data. If multiples are matched, then `advanceAmount` and `advanceUnit` will be used to advance the schedule data for each duplicate. The multiple files will be sorted alphabetically to produce a consistent result.

### advanceAmount

*number*

Default: `1`

The numerical amount to advance the `scheduleData` for each additional file that matches the pattern.

### advanceUnit

*"day" | "week" | "month" | "year"*

Defaults: `"week"`

The unit (as defined in [Moment](https://momentjs.com/)) to advance the time for each additional file that matches the pattern.
