import { Moment } from "moment";

/**
 * Describes the options for a single path.
 */
export interface PathSchedule
{
    /**
     * When should the file be published. If there are multiple ones, this is
     * the starting date for publishing.
     */
    scheduleData: string | Moment;

    /**
     * The amount to advance each additional entry. Defaults to 1 if not
     * provided.
     */
    advanceAmount?: number;

    /**
     * The unit to advance each additional entry. Defaults to "week" if not
     * provided.
     */
    advanceUnit?: "day" | "week" | "month" | "year";


    /**
     * If this is set, then copy the calculated date to a propery.
     */
    copyDateTo?: string;
}
