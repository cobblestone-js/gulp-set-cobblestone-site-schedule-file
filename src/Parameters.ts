/**
 * Describes the parameters that are given to the plugin.
 */
export interface Parameters
{
    /**
     * The required path to the schedule file, a YAML file.
     */
    scheduleFile?: string;

    /**
     * Optional YAML to use instead of a specific file.
     */
    scheduleYaml?: string;

    /**
     * The dotted notiation into the file for the schedule data array.
     * Defaults to "data.schedule".
     */
    property?: string;

    /**
     * If set to 1 or higher, then print additional logging statements while
     * processing.
     */
    logLevel?: number;

    /**
     * If provided, allows a callback for testing skip functionality.
     */
    skipIf?: { (file: any): boolean };
}
