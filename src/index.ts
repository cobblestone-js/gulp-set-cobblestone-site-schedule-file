import * as stream from "stream";
import * as fs from "fs";
import * as log from "fancy-log";
import * as through from "through2";
import { Parameters } from "./Parameters";
import { Schedule } from "./Schedule";
import * as yaml from "js-yaml";
import { PatternSchedule } from "./PatternSchedule";
import * as Moment from "moment";
import * as _ from "lodash";

/**
 * Process the files and gather up the ones that match one of the file patterns
 * that are in the site file.
 */
function gather(
    params: Parameters,
    schedules: PatternSchedule[],
    unpushed: any[],
    file: any,
    callback: any,
): void
{
    // If we don't have a relative path, then we don't do anything.
    if (!file.data || !file.relative)
    {
        return callback(null, file);
    }

    // Processing the file.
    const logLevel = params.logLevel || 0;

    if (logLevel > 4)
    {
        log("processing file", file.relative);
    }

    // See if we are skipping it.
    if (params.skipIf && params.skipIf(file))
    {
        return callback(null, file);
    }

    // See if this file matches any pattern we know about.
    const relativePath = file.relative;

    for (const schedule of schedules)
    {
        if (logLevel > 3)
        {
            log(
                "testing",
                relativePath,
                "against",
                schedule.pattern,
                schedule.pattern.test(relativePath));
        }

        if (schedule.pattern.test(relativePath))
        {
            unpushed.push(file);

            return callback(null);
        }
    }

    // We just use an empty callback because `flush` will write
    // out everything needed.
    callback(null, file);
}

/**
 * Called when the plugin has finished and the stream has completed. This is
 * where we process the files and handle any matches.
 *
 * @param done The callback after finishing the file.
 */
function flush(
    params: Parameters,
    schedules: PatternSchedule[],
    unpushed: any[],
    done: any,
    push: any,
): void
{
    // Loops through the schedules and gather each file.
    for (const schedule of schedules)
    {
        // Go through the files and sort them.
        var files = unpushed
            .filter((f: any) => schedule.pattern.test(f.relative));

        files.sort((a: any, b: any) =>
            a.relative.localeCompare(b.relative));

        // Figure out what we'll be adding to the schedule, which is arbitrary
        // data minus our set fields.
        let newData: any = { ...schedule.schedule };
        delete newData.scheduleData;
        delete newData.advanceAmount;
        delete newData.advanceUnit;
        delete newData.copyDateTo;

        // Pull out the scheduleDataing point.
        let scheduleData = Moment(schedule.schedule.scheduleData);
        const property = params.property || "data.schedule";
        const advanceAmount = schedule.schedule.advanceAmount || 1;
        const advanceUnit = schedule.schedule.advanceUnit || "week";
        const copyDateTo = schedule.schedule.copyDateTo;

        for (const file of files)
        {
            // Get the schedule data or an empty array.
            let data: any = _.get(file, property, []);

            // Create the schedule entry.
            const fileSchedule = {
                scheduleDate: scheduleData.format("YYYY-MM-DD"),
                ...newData,
            };

            // If we have a copy date, then set that.
            if (copyDateTo)
            {
                fileSchedule[copyDateTo] = scheduleData.format("YYYY-MM-DD");
            }

            // Push the results to the data.
            data.push(fileSchedule);

            // Set the file data and then push it into the stream.
            _.set(file, property, data);
            push(file);

            // Advance the scheduleData date in case we have multiples.
            scheduleData.add(advanceAmount, advanceUnit);
        }
    }

    // Finish up the flushing.
    done();
}

/**
 * Processes the files as they are entered through the Gulp stream, applying
 * schedule data as appropriate. Schedule files are held back until the flush
 * to determine if the regular expression matches more than one.
 *
 * @param  params [description]
 * @return        [description]
 */
function process(params: Parameters): stream.Transform
{
    // Make sure we have our parameters defined.
    if (!params)
    {
        throw new Error("The parameters are required for this plugin: { scheduleFile: '' }");
    }

    // Get the schedule.
    const schedules = loadSchedules(params);
    const logLevel = params.logLevel || 0;

    if (logLevel > 0)
    {
        log("schedules", schedules);
    }

    // Keep track of the files we haven't pushed because we are seeing if they
    // match multiple entries.
    var unpushed: any[] = [];

    // Return the resulting pipe.
    return through(
        { objectMode: true },
        function (file: any, _encoding: any, callback: any)
        {
            gather(params, schedules, unpushed, file, callback);
        },
        function (callback: any)
        {
            flush(params, schedules, unpushed, callback, (a: any) => this.push(a));
        });
}

/**
 * Loads the schedule file from either the file system or from the given string.
 *
 * @param  params The parameters to load the schedule.
 * @return        The resulting schedule file.
 */
function loadSchedules(params: Parameters): PatternSchedule[]
{
    // The provided YAML takes precendence when loading.
    let schedule: Schedule;

    if (params.scheduleYaml)
    {
        schedule = yaml.safeLoad(params.scheduleYaml) as Schedule;
    }
    else if (params.scheduleFile)
    {
        const fileData = fs.readFileSync(params.scheduleFile).toString();
        schedule = yaml.safeLoad(fileData) as Schedule;
    }
    else
    {
        // Otherwise, we need to blow up because we can't handle this.
        throw new Error("Either the parameter named `scheduleFile` or `scheduleYaml` is required");
    }

    // Once we have the schedule, we need to convert it into a list of patterns.
    let patterns: PatternSchedule[] = [];

    for (const id in schedule)
    {
        patterns.push({
            pattern: new RegExp(id),
            schedule: schedule[id],
        });
    }

    return patterns;
}

// Gulp wants a single function.
module.exports = process;
