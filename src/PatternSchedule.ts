import { PathSchedule } from "./PathSchedule";

/**
 * The interface describing the top-level schedule file.
 */
export interface PatternSchedule
{
    pattern: RegExp;
    schedule: PathSchedule;
}
