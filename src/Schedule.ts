import { PathSchedule } from "./PathSchedule";

/**
 * The interface describing the top-level schedule file.
 */
export interface Schedule
{
    [id: string]: PathSchedule;
}
