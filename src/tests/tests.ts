const plugin = require("../index");
import * as expect from "expect";
import "mocha";
import * as File from "vinyl";

describe("process files", function ()
{
    it("doesn't match a single file", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({
            scheduleYaml: `
            nomatch.md: { scheduleData: "2019-01-01" }
            `,
        });

        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input.md",
            data: { id: 1 } }));

        test(
            pipe,
            [
                {
                    id: 1,
                },
            ],
            done);
    });

    it("does match a single file", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({
            scheduleYaml: `
            input.md: { scheduleData: "2019-02-03", test: 1 }
            `,
        });

        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input.md",
            data: { id: 1 },
        }));

        test(
            pipe,
            [
                {
                    id: 1,
                    schedule: [{ scheduleDate: "2019-02-03", test: 1 }],
                },
            ],
            done
        );
    });

    it("does match multiple files", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({
            scheduleYaml: `
            input-\\d\\d.md: { scheduleData: "2019-02-03", test: 1, copyDateTo: postDate }
            `,
            copyDateTo: "postDate",
        });

        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input-01.md",
            data: { id: 1 } }));
        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input-02.md",
            data: { id: 2 },
        }));

        test(
            pipe,
            [
                {
                    id: 1,
                    schedule: [{
                        scheduleDate: "2019-02-03",
                        test: 1,
                        postDate: "2019-02-03",
                    }],
                },
                {
                    id: 2,
                    schedule: [{
                        scheduleDate: "2019-02-10",
                        test: 1,
                        postDate: "2019-02-10",
                    }],
                },
            ],
            done
        );
    });

    it("handles skipIf", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({
            scheduleYaml: `
                input-\\d\\d.md: { scheduleData: "2019-02-03", test: 1 }
            `,
            skipIf: (file: any) => file.relative === "input-02.md",
        });

        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input-01.md",
            data: { id: 1 },
        }));
        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input-02.md",
            data: { id: 2 },
        }));

        test(
            pipe,
            [
                {
                    id: 2,
                },
                {
                    id: 1,
                    schedule: [{ scheduleDate: "2019-02-03", test: 1 }],
                },
            ],
            done
        );
    });

    it("does match multiple files every two days", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({
            scheduleYaml: `
            input-\\d\\d.md:
              scheduleData: "2019-02-03"
              test: 1
              advanceAmount: 2
              advanceUnit: day
            `,
        });

        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input-01.md",
            data: { id: 1 },
        }));
        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input-02.md",
            data: { id: 2 },
        }));

        test(
            pipe,
            [
                {
                    id: 1,
                    schedule: [{ scheduleDate: "2019-02-03", test: 1 }],
                },
                {
                    id: 2,
                    schedule: [{ scheduleDate: "2019-02-05", test: 1 }],
                },
            ],
            done
        );
    });

    it("does match multiple files with multiple rules", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({
            scheduleYaml: `
            input-\\d\\d.md:
              scheduleData: "2019-02-03"
              test: 1
              advanceAmount: 2
              advanceUnit: day
            second-\\d[1-2].md:
              scheduleData: "2017-01-01"
              test: 2
            `,
        });

        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input-01.md",
            data: { relativePath: "input-01.md" },
        }));
        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input-02.md",
            data: { relativePath: "input-02.md" },
        }));
        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/input-03.md",
            data: { relativePath: "input-03.md" },
        }));
        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/second-01.md",
            data: { relativePath: "second-01.md" },
        }));
        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/second-02.md",
            data: { relativePath: "second-02.md" },
        }));
        pipe.write(new File({
            cwd: "/",
            base: "/test/",
            path: "/test/second-03.md",
            data: { relativePath: "second-03.md" },
        }));

        // Unmatched files get pushed up to the top.
        test(
            pipe,
            [
                {
                    relativePath: "second-03.md",
                },
                {
                    relativePath: "input-01.md",
                    schedule: [{ scheduleDate: "2019-02-03", test: 1 }],
                },
                {
                    relativePath: "input-02.md",
                    schedule: [{ scheduleDate: "2019-02-05", test: 1 }],
                },
                {
                    relativePath: "input-03.md",
                    schedule: [{ scheduleDate: "2019-02-07", test: 1 }],
                },
                {
                    relativePath: "second-01.md",
                    schedule: [{ scheduleDate: "2017-01-01", test: 2 }],
                },
                {
                    relativePath: "second-02.md",
                    schedule: [{ scheduleDate: "2017-01-08", test: 2 }],
                },
            ],
            done
        );
    });
});

function test(pipe: any, expected: any[], done: any)
{
    pipe.end();

    // Verify the results of the parsing.
    var output: any[] = [];

    pipe.on("data", function (file: any)
    {
        output.push(file.data);
    });

    pipe.on("end", function ()
    {
        expect(output).toEqual(expected);
        done();
    });
}
